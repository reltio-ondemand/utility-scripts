package com.reltio.domain;

import java.util.ArrayList;
import java.util.List;

import com.reltio.data.hcp.HcpObject;

public class DBScanResponse {
	Cursor cursor;
	
	
	List<HcpObject> objects = new ArrayList<HcpObject>();



	public Cursor getCursor() {
		return cursor;
	}


	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}


	public List<HcpObject> getObjects() {
		return objects;
	}


	public void setObjects(List<HcpObject> objects) {
		this.objects = objects;
	}


	
	
}
