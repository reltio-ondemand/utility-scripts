package com.reltio.test;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import au.com.bytecode.opencsv.CSVWriter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reltio.data.common.Attribute;
import com.reltio.data.hcp.HcpObject;
import com.reltio.domain.DBScanResponse;

public class CrosswalkExtractReport {

public static void main(String[] args) throws Exception{
	long startTime = System.currentTimeMillis();
	ReltioCoreFunction rcf = new ReltioCoreFunction();
	String propFilePath = args[0];
	Properties properties = new Properties();
    FileReader in = new FileReader(propFilePath);
    properties.load(in);
	
	String authUrl = properties.getProperty("AUTHURL");//"https://auth.reltio.com";
	String username = properties.getProperty("USERNAME");
	String password = properties.getProperty("PASSWORD");
	String apiURL = properties.getProperty("APIURL");
	String entityType = properties.getProperty("ENTITYTYPE");
	String outputPath = properties.getProperty("OUTPUTPATH");
	String OAUTH_CLIENT_ID = properties.getProperty("OAUTH_CLIENT_ID");
	String OAUTH_CLIENT_SECRET = properties.getProperty("OAUTH_CLIENT_SECRET");
	
	Map<String,String> params = new HashMap<String,String>();
	rcf.getAccessToken(authUrl, username, password,OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET);
	Gson gson = new GsonBuilder().create();
	BufferedWriter output = new BufferedWriter(new FileWriter (outputPath));
	CSVWriter csvWriter = new CSVWriter(output);
	String[] outputValues = new String[3];
	String scanUrl = apiURL + "/entities/_scan?filter=(equals(type,%27configuration/entityTypes/"+entityType+"%27))&max=200";
	boolean flag = true;
	int cnt = 0;
	String requestString = "";
	String cursor = "";
	outputValues[0] = "EntityURI";
	outputValues[1] = "Source";
	outputValues[2] = "Crosswalk";
	
	csvWriter.writeNext(outputValues);
	csvWriter.flush();
	
	while(flag){
		System.out.println(cnt);
		if(cnt==0)
			requestString = "";
		else
			requestString = "{\"cursor\": {  \"value\": \""+cursor+"\"}}";
			
		String scanResponse = rcf.sendAPICall(authUrl, scanUrl,OAUTH_CLIENT_ID,OAUTH_CLIENT_SECRET, requestString, username, password, "POST", params);
		DBScanResponse scanResponseObj = gson.fromJson(scanResponse, DBScanResponse.class);
		//System.out.println("scanResponse="+scanResponse);
		if(scanResponseObj != null ){
		List<HcpObject> hcpObjectsList = scanResponseObj.getObjects();
		if(hcpObjectsList != null && !hcpObjectsList.isEmpty()){
		cursor = scanResponseObj.getCursor().getValue();
		for (HcpObject hcpObj : hcpObjectsList) {
				List<Attribute> cws = hcpObj.crosswalks;
				//int cwCnt = 0;
				
				for (Attribute cwAttributes : cws) {
						try{
								outputValues[0] = hcpObj.uri.substring(9);
								outputValues[1] = cwAttributes.type.substring(cwAttributes.type.lastIndexOf("/")+1);
								outputValues[2] = cwAttributes.value;
								csvWriter.writeNext(outputValues);
								csvWriter.flush();
						}catch(Exception e){
							e.printStackTrace();
						}
					outputValues[0] = "";
					outputValues[1] = "";
					outputValues[2] = "";
				}
		}
		}else{
			flag = false;
		}
		}
		cnt++;
	}System.out.println("Report generated");
	long endTime   = System.currentTimeMillis();
	//long totalTime = endTime - startTime;
	NumberFormat formatter = new DecimalFormat("#0.00000");
	System.out.print("Execution time is " + formatter.format((endTime - startTime) / 1000d) + " seconds");
	csvWriter.close();
	}
}

