package com.reltio.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Ganesh.Palanisamy@reltio.com Created : Sep 19, 2014
 */
public class ReltioObject {

	public String type;
	public String uri;
	public String label;
	
	public Map<String, List<Object>> attributes = new HashMap<String, List<Object>>();
	
	public List<Crosswalk> crosswalks;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Map<String, List<Object>> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, List<Object>> attributes) {
		this.attributes = attributes;
	}

	public List<Crosswalk> getCrosswalks() {
		return crosswalks;
	}

	public void setCrosswalks(List<Crosswalk> crosswalks) {
		this.crosswalks = crosswalks;
	}
	

}
