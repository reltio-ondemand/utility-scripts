package com.reltio.test;

public class Attribute1 {

	private String value;
	private String cursor;

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the cursor
	 */
	public String getCursor() {
		return cursor;
	}

	/**
	 * @param cursor
	 *            the cursor to set
	 */
	public void setCursor(String cursor) {
		this.cursor = cursor;
	}

}
