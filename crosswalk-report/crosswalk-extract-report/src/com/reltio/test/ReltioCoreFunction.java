/*******************************************************************************
 * Copyright 2014 Reltio. All Rights Reserved.
 *******************************************************************************/
package com.reltio.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;


/**
 * 
 * This is the utility class for calling the Reltio API Services.
 * 
 * @author Ganesh
 * 
 */
public class ReltioCoreFunction {

	private static String authToken = "";
	/**
     *    This method is to get access token
     * @param authrl
     * @param username
     * @param password
     * @param oauth_client_id
     * @param oauth_client_secret
     * @return
     * @throws Exception
     */
    public static void getAccessToken(String authURL, String username, String password, String oauth_client_id, String oauth_client_secret) throws Exception {
        URLConnection connection = new URL(authURL+"/oauth/token?username="+username+"&password="+password+"&grant_type=password").openConnection();
        connection.setRequestProperty("Authorization", "Basic "+Base64.encodeBase64String((oauth_client_id + ":" + oauth_client_secret).getBytes("UTF-8")));
        InputStream response = null;
        StringBuilder builder = new StringBuilder();
        try {
            response = connection.getInputStream();
          
            byte[] buffer = new byte[100000];
            int count = response.read(buffer);

            while(count > 0) {
                builder.append(new String(buffer, 0, count));
                count = response.read(buffer);
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }
        
        String responseStr = builder.toString();
        int index = responseStr.indexOf("\"access_token\":\"");
        if (index >= 0) {
            int endIndex = responseStr.indexOf("\"", index + "\"access_token\":\"".length());
            String access_token = responseStr.substring(index + "\"access_token\":\"".length(), endIndex);
            authToken =	access_token;
        }
    }

	/**
	 * THis method used to POST/PUT any request to the reltio API
	 * 
	 * @param apiurl
	 * @param body
	 * @param userName
	 * @param password
	 * @param requestMethod
	 * @return
	 * @throws Exception
	 */
	public static String sendAPICall(String authUrl, String apiUrl, String OAUTH_CLIENT_ID, String OAUTH_CLIENT_SECRET, String body, String userName, String password, String requestMethod)
			throws Exception {
		
		URL url = new URL(apiUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod(requestMethod);
		connection.setRequestProperty("Authorization", "Bearer " + authToken);
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("charset", "UTF-8");
//		connection.setRequestProperty("Source-System", "configuration/sources/HCOS");
		
		OutputStream output = null;
		if(!"GET".equals(requestMethod)){
			connection.setDoOutput(true);
				output = connection.getOutputStream();
		
				try {
		
					output.write(body.getBytes("UTF-8"));
				}catch(Exception e){
					e.printStackTrace();
				} 
				finally {
					try {
						output.close();
					} catch (IOException logOrIgnore) {
					}
				}
		}
		InputStream response = null;

		int errorCode = connection.getResponseCode();
		int iteration = 0;
		while (errorCode >= 400) {
			Date date = new Date();
			iteration++;
			System.out.println("ErrorCode:" + errorCode + "|Iteration:"
					+ iteration + "|" + apiUrl + "|" + date.toString());
			if (iteration == 5) {
				System.out.println("Failed To Do the Scan....");
				break;
			}
			getAccessToken(authUrl, userName, password, OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod(requestMethod);
			connection.setRequestProperty("Authorization", "Bearer " + authToken);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("charset", "UTF-8");
			if(!"GET".equals(requestMethod)){
				output = connection.getOutputStream();
	
				try {
	
					output.write(body.getBytes("UTF-8"));
				} finally {
					try {
						output.close();
					} catch (IOException logOrIgnore) {
					}
				}
			}
			errorCode = connection.getResponseCode();
		}

		StringBuilder builder = new StringBuilder();
		try {
			response = connection.getInputStream();

			byte[] buffer = new byte[100000];
			int count = response.read(buffer);
			while (count > 0) {
				builder.append(new String(buffer, 0, count));
				count = response.read(buffer);
			}
		} finally {
			if (response != null) {
				response.close();
			}
		}

		return builder.toString();

	}
	public static String sendAPICall(String authUrl, String apiUrl,String OAUTH_CLIENT_ID,String OAUTH_CLIENT_SECRET, String body, String userName, String password, String requestMethod, Map<String, String> reqParams)
			throws Exception {
		
		URL url = new URL(apiUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod(requestMethod);
		connection.setRequestProperty("Authorization", "Bearer " + authToken);
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("charset", "UTF-8");
		for(String key : reqParams.keySet()){
			connection.setRequestProperty(key, reqParams.get(key));
		}
		
		OutputStream output = null;
		if(!"GET".equals(requestMethod)){
			connection.setDoOutput(true);
				output = connection.getOutputStream();
		
				try {
		
					output.write(body.getBytes("UTF-8"));
				}catch(Exception e){
					e.printStackTrace();
				} 
				finally {
					try {
						output.close();
					} catch (IOException logOrIgnore) {
					}
				}
		}
		InputStream response = null;

		int errorCode = connection.getResponseCode();
		int iteration = 0;
		while (errorCode >= 400) {
			Date date = new Date();
			iteration++;
			System.out.println("ErrorCode:" + errorCode + "|Iteration:"
					+ iteration + "|" + apiUrl + "|" + date.toString());
			if (iteration == 5) {
				System.out.println("Failed To Do the Scan....");
				break;
			}
			getAccessToken(authUrl, userName, password, OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod(requestMethod);
			connection.setRequestProperty("Authorization", "Bearer " + authToken);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("charset", "UTF-8");
			for(String key : reqParams.keySet()){
				connection.setRequestProperty(key, reqParams.get(key));
			}
			if(!"GET".equals(requestMethod)){
				output = connection.getOutputStream();
	
				try {
	
					output.write(body.getBytes("UTF-8"));
				} finally {
					try {
						output.close();
					} catch (IOException logOrIgnore) {
					}
				}
			}
			errorCode = connection.getResponseCode();
		}

		StringBuilder builder = new StringBuilder();
		try {
			response = connection.getInputStream();

			byte[] buffer = new byte[100000];
			int count = response.read(buffer);
			while (count > 0) {
				builder.append(new String(buffer, 0, count));
				count = response.read(buffer);
			}
		} finally {
			if (response != null) {
				response.close();
			}
		}

		return builder.toString();

	}

	


	 public static String deleteRecord(String authUrl,String apiURL,String OAUTH_CLIENT_ID,String OAUTH_CLIENT_SECRET,String username, String password) throws Exception {
//		 return apiURL;
		 
	        
	        URL url = new URL(apiURL);
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	        // Set as DELETE request
	        conn.setDoOutput(true);
	        conn.setRequestProperty("Authorization", "Bearer "  +  authToken);
	        conn.setRequestProperty("Source-System","configuration/sources/HCOS");
	        conn.setRequestProperty("Content-Type", "application/json");
	        conn.setRequestMethod("DELETE");
	        conn.connect();      
	        
	        int iteration =0;
	        int errorCode =conn.getResponseCode();
	        while ( errorCode >= 400 ) {
	        		Date date = new Date();
	    			iteration++;
	    			System.out.println("ErrorCode:"+errorCode+"|Iteration:"+iteration+"|"+Thread.currentThread().getName()+ "|"+date.toString()+"|"+apiURL);
	                if (iteration==5) {
	                	System.out.println("FailedJsonToResend_Exception:"+apiURL);
	                	break;
	                }
	            	conn = (HttpURLConnection) url.openConnection();
	                conn.setDoOutput(true);
	                getAccessToken(authUrl,username,password,OAUTH_CLIENT_ID,OAUTH_CLIENT_SECRET);
	                conn.setRequestProperty("Authorization", "Bearer "  + authToken);
	                conn.setRequestProperty("Content-Type", "application/json");
	                conn.setRequestMethod("DELETE");
	                conn.connect();
	               
	                errorCode =conn.getResponseCode(); 
	                
	        }

			String responseStr = conn.getResponseMessage().toString();	
	        return responseStr;
	       
	    }
	

}
